package classandobjects;

//Class : blue print of how to create object
//Object: instance of method
public class Animal {
    //State(variable)
    String name;
    int health;

    //Behaviors(method)



    /* [return type] methodName (parameters){
            // logic would go inside the method
            }
     */

    /*
     a method is reusable code that is only ran when called

     methods are functions tied to class, functions aren't tied to anything
     */
    int heal(int amount){
        health = health + amount;
        return health;
    }

    //Constructor

    Animal(){
        System.out.println("No Arg Constructor");
    }
    Animal(String word){
        System.out.println("Constructor with argument" + word);
    }

    // initializer block
    {
        System.out.println("instance block");
    }

    //static block
    static{
        System.out.println("In static block");
    }
}
