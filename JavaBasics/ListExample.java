package Basics;

import java.util.*;

/*
    Attributes of a List:
        -stores an ordered collection of like objects
        - can have duplicate values
 */
public class ListExample {
    public static void main(String[] args){

        /*
            Array List utilizes arrays
         */

        //You can't uses primitive data types with Collection
        List<Integer> arrayList = new ArrayList<>();
        arrayList.add(1) ;
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);

       // arrayList.remove(1);
        arrayList.remove(new Integer(3));

        System.out.println(arrayList);
        System.out.println(arrayList.size());
        System.out.println(arrayList.isEmpty());

        List<Integer> vector = new Vector<>();
        vector.add(1);
        vector.add(2);
        vector.add(3);
        vector.add(3);
        vector.add(4);

        vector.remove(2);

        System.out.println(vector.get(2));

        List<Integer> linkedList = new LinkedList<>();
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(4);

        linkedList.remove(2);

        System.out.println(linkedList);

        /*
            Ways to iterate through collections
                -Iterator
                -Lambdas(forEach method)
                -enhanced for loop
         */

        for(Integer x : linkedList){
            System.out.println(x + ", ");
        }

        Iterator<Integer> listIterator = arrayList.listIterator();

        while(listIterator.hasNext()){
            System.out.print(listIterator.next());
        }

        System.out.println();

        //Lambda
        arrayList.forEach(x -> System.out.println(x));

        List<List<Integer>>arrOfArray = new ArrayList<>();
        List<Integer> arr1 = new ArrayList<>();
        List<Integer> arr2 = new ArrayList<>();

        arr1.add(1);
        arr1.add(1);
        arr1.add(2);
        arr1.add(2);

        arrOfArray.add(arr1);
        arrOfArray.add(arr2);

        System.out.println(arrOfArray);

    }
}
