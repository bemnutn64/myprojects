package Basics;

public class Main {
    public static void main(String[] args){
        System.out.println("Hello Java");

        //whole numbers
        boolean bool = true;
        short s = 20;
        int i = 3;
        long l = 50;
        double d = 3.14;
        float f = 3.14f;
        byte b = 10;
        char c = 'c';

        //Arrays
        int[] arr= {1,2,3,4,5};
        int[] arr2 = new int[99];

        System.out.println(arr[2]);

        System.out.println(arr2[2]);
        arr2[2] = 30;
        System.out.println(arr2[2]);

        //Control Flow

        //if else statement

        int cFlow = 60;

        if(cFlow > 70) {
            System.out.println("inside of if");

        }else if(cFlow > 50){
            System.out.println("inside of if else");
        }
        else{
            System.out.println("in else");
        }
        // for loop
        int[] forExample = {0,1,2,3,4,5};

        int plusPlus = 0;
        System.out.println(++plusPlus);

        for(int x = 0; x < forExample.length; x++){
            System.out.println(forExample[x]);
        }

        //reverse for loop

        for(int x = forExample.length - 1; x >= 0; x--){
            System.out.print(forExample[x]);
        }

        System.out.println();

        //Enhanced for loop
        for(int x : forExample){
            System.out.print(x);
        }

            System.out.println();
        //switch
        int x = 5;

        switch(x){
            case 5:
                System.out.println("5!!!!!");
            case 10:
                System.out.println("10!!!!!");
                break;
            default:
                System.out.println("Not 5 or 10");
        }

        //while
        int ex = 0;
        while(ex < 10){
            System.out.println(ex++);
        }

        // do-while
        ex= 0;

        do{
            System.out.println(ex);
        }while(ex < 0);


    }
}
