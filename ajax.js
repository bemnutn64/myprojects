/* This function runs window */
window.onload = function() {
    //console.log("ITS CONNECTED");

    // XMLHttpRequest Object
    var xhttp = new XMLHttpRequest()

    /* step 2. create the readystate function for the ready state changes
    
        has several states
            - state 0; not initialized
            - state 1: server connection has been established
            - state 2: request recieved 
            - state 2: processing request
            - state 4: complete, request
    */

    xhttp.onreadystatechange = function(){ 
        if(xhttp.readyState === 4){
            console.log('ready state is changing')
            let response = JSON.parse(xhttp.responseText)
            console.log(response)
        }
    }

    // step 3. create a connection

    xhttp.open('GET', 'https://pokeapi.co/api/v2/pokemon/zapdos')

    // step 4. send the request
    xhttp.send()



    /* fetch api */
    console.log("before fetch")
    fetch('https://pokeapi.co/api/v2/pokemon/zapdos')
    .then(function(response){
        var jsonResponse = response.json()
        return jsonResponse;
    })
    .then(function(data){
        console.log(data)
    })
    console.log("after fetch")

    getZapdos()
}

/* async await */

async function getZapdos(){
    console.log("before  async await")
    let response = await fetch('https://pokeapi.co/api/v2/pokemon/zapdos')
    let data = await response.json();

    console.log(data)
    console.log("after  async await")
}



/* comment SHIFT+ALT+A */

/* AJAX- Asynchronous JavaScript and XML 

    Asynchronous means 'non-blocking'
        -a task that can be performed with other tasks

    This is how we send HTTP requests

    3 ways to send HTTPRequests in JavaScript
        1. XMLHTTPRequest Object
        2. fetch api
        3. async await
*/